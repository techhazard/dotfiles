#############################
# Makefile to sync dotfiles #
#############################
SHELL=/bin/bash

all: pull update


pull:
	@git pull -r origin master 2>&1 1>/dev/null


clean:
	@./clean
	@cd ${HOME} && rm -rf ${HOME}/.dotfiles


.PHONY: pull clean all update 

update:

	@./update
 
