# Repo for config files

## Installing

```
git clone https://bitbucket.org/awesomefireduck/dotfiles.git $HOME/.config/dotfiles \
&& make -C $HOME/.config/dotfiles
```

## Adding a repo

use an uri to a git repo, can be HTTP(S) or SHH

```
./insert [repo uri]
```
this will make a .get file with the name of the repo containing the URI in the root of this folder.

#### example:  
```
./insert git@bitbucket.org:awesomefireduck/neovim.git
```
will make a file named `neovim.get` containing the line `git@bitbucket.org:awesomefireduck/neovim.git` in the `~/.config/dotfiles` directory.


## Updating repos

running `make` in the repo will do the following:


* update this repo: `git pull -r`
* for every file in `*.get`:
	- clone repo in ~/.config if nonexistant
	- run `make` in repo

The repo will be cloned in a folder in $HOME/.config


## Autorun script in repo

You can add a `Makefile` that will be run whenever update is run


#### example:
(see the neovim.get file in the example above)
```
git clone git@bitbucket.org:awesomefireduck/neovim.git
```
This will create the `~/.config/neovim` directory.